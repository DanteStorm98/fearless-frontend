function createCard(name, description, pictureUrl, datestart, dateend, loc) {
    const startdate = new Date(datestart).toLocaleDateString('en-US')
    const enddate = new Date(dateend).toLocaleDateString('en-US')

    return `
    <div class="card shadow-md p-3 mb-5 bg-body-tertiary rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
      <h5 class="card-title">${loc}</h5>
        <h5 class="card-title">${name}</h5>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">${startdate} - ${enddate}</div>
    </div>`;
  }
function alertMessage(){
  return `
      <div class="alert alert-danger" role="alert">
      Bad request!
      </div>
      `

}
window.addEventListener("DOMContentLoaded", async () => {


  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      const hmtl2 = alertMessage();
      const main =document.querySelector('main');
      main.innerHTML += hmtl2;
      // console.error("Nah Bruh");
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();
      let count = 0;

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const datestart = details.conference.starts;
          const dateend = details.conference.ends;
          const loc = details.conference.location.name;
          const pictureUrl = details.conference.location.picture_url;
          const html = createCard(title, description, pictureUrl, datestart, dateend, loc);
          //console.log(html);

          const column = document.querySelectorAll('.col');
          column[count].innerHTML += html;
          count += 1
          if (count === column.length) {
            count = 0
          }
        }
      }
    }
  } catch (e) {
    // Figure out what to do if an error is raised
    console.error("Nah bruh", e);
  }
});
